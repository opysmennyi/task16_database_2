-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`biography`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`biography` (
  `id_bio` INT(11) NOT NULL,
  `photo` VARCHAR(45) NULL DEFAULT NULL,
  `biography` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id_bio`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`oblast`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`oblast` (
  `code_oblast` INT(11) NOT NULL,
  `oblast` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`code_oblast`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`city`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`city` (
  `city` VARCHAR(45) NOT NULL,
  `oblast_code_oblast` INT(11) NOT NULL,
  PRIMARY KEY (`city`),
  INDEX `fk_city_oblast1_idx` (`oblast_code_oblast` ASC) VISIBLE,
  CONSTRAINT `fk_city_oblast1`
    FOREIGN KEY (`oblast_code_oblast`)
    REFERENCES `mydb`.`oblast` (`code_oblast`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`scholarship`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`scholarship` (
  `Y` INT(11) NOT NULL,
  `N` VARCHAR(45) NULL DEFAULT NULL,
  `student_id` INT(11) NOT NULL,
  `student_home_address` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Y`, `student_id`, `student_home_address`),
  INDEX `fk_scholarship_student1_idx` (`student_id` ASC, `student_home_address` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`score`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`score` (
  `score` VARCHAR(45) NOT NULL,
  `scholarship_Y` INT(11) NOT NULL,
  `scholarship_student_id` INT(11) NOT NULL,
  `scholarship_student_home_address` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`score`, `scholarship_Y`, `scholarship_student_id`, `scholarship_student_home_address`),
  INDEX `fk_score_scholarship1_idx` (`scholarship_Y` ASC, `scholarship_student_id` ASC, `scholarship_student_home_address` ASC) VISIBLE,
  CONSTRAINT `fk_score_scholarship1`
    FOREIGN KEY (`scholarship_Y` , `scholarship_student_id` , `scholarship_student_home_address`)
    REFERENCES `mydb`.`scholarship` (`Y` , `student_id` , `student_home_address`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`mark`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`mark` (
  `idmark` INT(11) NOT NULL,
  `mark_for_m1` VARCHAR(45) NULL DEFAULT NULL,
  `mark_for_m2` VARCHAR(45) NULL DEFAULT NULL,
  `m_semester_hundred` VARCHAR(45) NULL DEFAULT NULL,
  `m_semestr_five` VARCHAR(45) NULL DEFAULT NULL,
  `semestr_idsemestr` INT(11) NOT NULL,
  `control_id_control` INT(11) NOT NULL,
  `module_id` INT(11) NOT NULL,
  `score_score` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idmark`, `semestr_idsemestr`, `control_id_control`, `module_id`, `score_score`),
  INDEX `fk_mark_semestr1_idx` (`semestr_idsemestr` ASC) VISIBLE,
  INDEX `fk_mark_control1_idx` (`control_id_control` ASC) VISIBLE,
  INDEX `fk_mark_module1_idx` (`module_id` ASC) VISIBLE,
  INDEX `fk_mark_score1_idx` (`score_score` ASC) VISIBLE,
  CONSTRAINT `fk_mark_score1`
    FOREIGN KEY (`score_score`)
    REFERENCES `mydb`.`score` (`score`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`control`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`control` (
  `id_control` INT(11) NOT NULL,
  `exam` VARCHAR(45) NULL DEFAULT NULL,
  `zalik` VARCHAR(45) NULL DEFAULT NULL,
  `controlcol` VARCHAR(45) NULL DEFAULT NULL,
  `mark_idmark` INT(11) NOT NULL,
  `mark_semestr_idsemestr` INT(11) NOT NULL,
  `mark_control_id_control` INT(11) NOT NULL,
  `mark_module_id` INT(11) NOT NULL,
  PRIMARY KEY (`id_control`, `mark_idmark`, `mark_semestr_idsemestr`, `mark_control_id_control`, `mark_module_id`),
  INDEX `fk_control_mark1_idx` (`mark_idmark` ASC, `mark_semestr_idsemestr` ASC, `mark_control_id_control` ASC, `mark_module_id` ASC) VISIBLE,
  CONSTRAINT `fk_control_mark1`
    FOREIGN KEY (`mark_idmark` , `mark_semestr_idsemestr` , `mark_control_id_control` , `mark_module_id`)
    REFERENCES `mydb`.`mark` (`idmark` , `semestr_idsemestr` , `control_id_control` , `module_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`debt`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`debt` (
  `debt` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`debt`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`graduated_vnz`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`graduated_vnz` (
  `id` INT(11) NOT NULL,
  `vnz_name` VARCHAR(45) NULL DEFAULT NULL,
  `phone_number` VARCHAR(45) NULL DEFAULT NULL,
  `dirctor` VARCHAR(45) NULL DEFAULT NULL,
  `city_city` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`, `city_city`),
  INDEX `fk_graduated_vnz_city1_idx` (`city_city` ASC) VISIBLE,
  CONSTRAINT `fk_graduated_vnz_city1`
    FOREIGN KEY (`city_city`)
    REFERENCES `mydb`.`city` (`city`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`specialitie`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`specialitie` (
  `id_spec` INT(11) NOT NULL,
  `specialitie` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id_spec`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`semestr`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`semestr` (
  `idsemestr` INT(11) NOT NULL,
  PRIMARY KEY (`idsemestr`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`group` (
  `group_number` INT(11) NOT NULL,
  `groupe_name` VARCHAR(45) NULL DEFAULT NULL,
  `year_of_enroll` VARCHAR(45) NULL DEFAULT NULL,
  `specialitie_id_spec` INT(11) NOT NULL,
  `semestr_idsemestr` INT(11) NOT NULL,
  PRIMARY KEY (`group_number`, `specialitie_id_spec`, `semestr_idsemestr`),
  INDEX `fk_group_specialitie1_idx` (`specialitie_id_spec` ASC) VISIBLE,
  INDEX `fk_group_semestr1_idx` (`semestr_idsemestr` ASC) VISIBLE,
  CONSTRAINT `fk_group_specialitie1`
    FOREIGN KEY (`specialitie_id_spec`)
    REFERENCES `mydb`.`specialitie` (`id_spec`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_group_semestr1`
    FOREIGN KEY (`semestr_idsemestr`)
    REFERENCES `mydb`.`semestr` (`idsemestr`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`home`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`home` (
  `address` VARCHAR(45) NOT NULL,
  `city_city` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`address`, `city_city`),
  INDEX `fk_home_city1_idx` (`city_city` ASC) VISIBLE,
  CONSTRAINT `fk_home_city1`
    FOREIGN KEY (`city_city`)
    REFERENCES `mydb`.`city` (`city`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`lessons`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`lessons` (
  `lessons_pass` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`lessons_pass`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`lessons_has_teacher`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`lessons_has_teacher` (
  `lessons_lessons_pass` VARCHAR(45) NOT NULL,
  `teacher_idteacher` INT(11) NOT NULL,
  PRIMARY KEY (`lessons_lessons_pass`, `teacher_idteacher`),
  INDEX `fk_lessons_has_teacher_teacher1_idx` (`teacher_idteacher` ASC) VISIBLE,
  INDEX `fk_lessons_has_teacher_lessons1_idx` (`lessons_lessons_pass` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`module`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`module` (
  `id` INT(11) NOT NULL,
  `teacher_idteacher` INT(11) NOT NULL,
  `teacher_student_id` INT(11) NOT NULL,
  `teacher_student_home_address` VARCHAR(45) NOT NULL,
  `mark_idmark` INT(11) NOT NULL,
  `mark_semestr_idsemestr` INT(11) NOT NULL,
  `mark_control_id_control` INT(11) NOT NULL,
  `mark_module_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `teacher_idteacher`, `teacher_student_id`, `teacher_student_home_address`, `mark_idmark`, `mark_semestr_idsemestr`, `mark_control_id_control`, `mark_module_id`),
  INDEX `fk_module_teacher1_idx` (`teacher_idteacher` ASC, `teacher_student_id` ASC, `teacher_student_home_address` ASC) VISIBLE,
  INDEX `fk_module_mark1_idx` (`mark_idmark` ASC, `mark_semestr_idsemestr` ASC, `mark_control_id_control` ASC, `mark_module_id` ASC) VISIBLE,
  CONSTRAINT `fk_module_mark1`
    FOREIGN KEY (`mark_idmark` , `mark_semestr_idsemestr` , `mark_control_id_control` , `mark_module_id`)
    REFERENCES `mydb`.`mark` (`idmark` , `semestr_idsemestr` , `control_id_control` , `module_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`student`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`student` (
  `student_card` INT(11) NOT NULL,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  `surname` VARCHAR(45) NULL DEFAULT NULL,
  `raiting` VARCHAR(45) NULL DEFAULT NULL,
  `date_of_birth` VARCHAR(45) NULL DEFAULT NULL,
  `date_of_enroll` VARCHAR(45) NULL DEFAULT NULL,
  `email` VARCHAR(45) NULL DEFAULT NULL,
  `graduated_vnz_id` INT(11) NOT NULL,
  `group_group_number` INT(11) NOT NULL,
  `full_name` VARCHAR(45) GENERATED ALWAYS AS (concat(`name`,_utf8mb3' ',`surname`)) VIRTUAL,
  `group_group_number1` INT(11) NOT NULL,
  `biography` VARCHAR(45) NULL,
  `home_address` VARCHAR(45) NOT NULL,
  `home_city_city` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`student_card`, `group_group_number1`),
  INDEX `fk_student_graduated_vnz1_idx` (`graduated_vnz_id` ASC) VISIBLE,
  INDEX `fk_student_group1_idx` (`group_group_number` ASC) VISIBLE,
  INDEX `fk_student_group1_idx1` (`group_group_number1` ASC) VISIBLE,
  INDEX `fk_student_home1_idx` (`home_address` ASC, `home_city_city` ASC) VISIBLE,
  CONSTRAINT `fk_student_graduated_vnz1`
    FOREIGN KEY (`graduated_vnz_id`)
    REFERENCES `mydb`.`graduated_vnz` (`id`),
  CONSTRAINT `fk_student_group1`
    FOREIGN KEY (`group_group_number1`)
    REFERENCES `mydb`.`group` (`group_number`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_home1`
    FOREIGN KEY (`home_address` , `home_city_city`)
    REFERENCES `mydb`.`home` (`address` , `city_city`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`student_has_debt`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`student_has_debt` (
  `student_student_card` INT(11) NOT NULL,
  `debt_debt` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`student_student_card`, `debt_debt`),
  INDEX `fk_student_has_debt1_debt1_idx` (`debt_debt` ASC) VISIBLE,
  INDEX `fk_student_has_debt1_student1_idx` (`student_student_card` ASC) VISIBLE,
  CONSTRAINT `fk_student_has_debt1_debt1`
    FOREIGN KEY (`debt_debt`)
    REFERENCES `mydb`.`debt` (`debt`),
  CONSTRAINT `fk_student_has_debt1_student1`
    FOREIGN KEY (`student_student_card`)
    REFERENCES `mydb`.`student` (`student_card`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`student_has_debt1`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`student_has_debt1` (
  `student_student_card` INT(11) NOT NULL,
  `debt_debt` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`student_student_card`, `debt_debt`),
  INDEX `fk_student_has_debt1_debt1_idx` (`debt_debt` ASC) VISIBLE,
  INDEX `fk_student_has_debt1_student1_idx` (`student_student_card` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`student_has_module`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`student_has_module` (
  `student_id` INT(11) NOT NULL,
  `module_id` INT(11) NOT NULL,
  PRIMARY KEY (`student_id`, `module_id`),
  INDEX `fk_student_has_module_module1_idx` (`module_id` ASC) VISIBLE,
  INDEX `fk_student_has_module_student1_idx` (`student_id` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`teacher`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`teacher` (
  `idteacher` INT(11) NOT NULL,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  `surname` VARCHAR(45) NULL DEFAULT NULL,
  `student_id` INT(11) NOT NULL,
  `student_home_address` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idteacher`, `student_id`, `student_home_address`),
  INDEX `fk_teacher_student1_idx` (`student_id` ASC, `student_home_address` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`group_has_lessons`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`group_has_lessons` (
  `group_group_number` INT(11) NOT NULL,
  `group_specialitie_id_spec` INT(11) NOT NULL,
  `group_semestr_idsemestr` INT(11) NOT NULL,
  `lessons_lessons_pass` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`group_group_number`, `group_specialitie_id_spec`, `group_semestr_idsemestr`, `lessons_lessons_pass`),
  INDEX `fk_group_has_lessons_lessons1_idx` (`lessons_lessons_pass` ASC) VISIBLE,
  INDEX `fk_group_has_lessons_group1_idx` (`group_group_number` ASC, `group_specialitie_id_spec` ASC, `group_semestr_idsemestr` ASC) VISIBLE,
  CONSTRAINT `fk_group_has_lessons_group1`
    FOREIGN KEY (`group_group_number` , `group_specialitie_id_spec` , `group_semestr_idsemestr`)
    REFERENCES `mydb`.`group` (`group_number` , `specialitie_id_spec` , `semestr_idsemestr`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_group_has_lessons_lessons1`
    FOREIGN KEY (`lessons_lessons_pass`)
    REFERENCES `mydb`.`lessons` (`lessons_pass`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`module_has_group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`module_has_group` (
  `module_id` INT(11) NOT NULL,
  `module_teacher_idteacher` INT(11) NOT NULL,
  `module_teacher_student_id` INT(11) NOT NULL,
  `module_teacher_student_home_address` VARCHAR(45) NOT NULL,
  `group_group_number` INT(11) NOT NULL,
  `group_specialitie_id_spec` INT(11) NOT NULL,
  `group_semestr_idsemestr` INT(11) NOT NULL,
  PRIMARY KEY (`module_id`, `module_teacher_idteacher`, `module_teacher_student_id`, `module_teacher_student_home_address`, `group_group_number`, `group_specialitie_id_spec`, `group_semestr_idsemestr`),
  INDEX `fk_module_has_group_group1_idx` (`group_group_number` ASC, `group_specialitie_id_spec` ASC, `group_semestr_idsemestr` ASC) VISIBLE,
  INDEX `fk_module_has_group_module1_idx` (`module_id` ASC, `module_teacher_idteacher` ASC, `module_teacher_student_id` ASC, `module_teacher_student_home_address` ASC) VISIBLE,
  CONSTRAINT `fk_module_has_group_module1`
    FOREIGN KEY (`module_id` , `module_teacher_idteacher` , `module_teacher_student_id` , `module_teacher_student_home_address`)
    REFERENCES `mydb`.`module` (`id` , `teacher_idteacher` , `teacher_student_id` , `teacher_student_home_address`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_module_has_group_group1`
    FOREIGN KEY (`group_group_number` , `group_specialitie_id_spec` , `group_semestr_idsemestr`)
    REFERENCES `mydb`.`group` (`group_number` , `specialitie_id_spec` , `semestr_idsemestr`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`control_has_group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`control_has_group` (
  `control_id_control` INT(11) NOT NULL,
  `group_group_number` INT(11) NOT NULL,
  `group_specialitie_id_spec` INT(11) NOT NULL,
  `group_semestr_idsemestr` INT(11) NOT NULL,
  `teacher_idteacher` INT(11) NOT NULL,
  `teacher_student_id` INT(11) NOT NULL,
  `teacher_student_home_address` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`control_id_control`, `group_group_number`, `group_specialitie_id_spec`, `group_semestr_idsemestr`, `teacher_idteacher`, `teacher_student_id`, `teacher_student_home_address`),
  INDEX `fk_control_has_group_group1_idx` (`group_group_number` ASC, `group_specialitie_id_spec` ASC, `group_semestr_idsemestr` ASC) VISIBLE,
  INDEX `fk_control_has_group_control1_idx` (`control_id_control` ASC) VISIBLE,
  INDEX `fk_control_has_group_teacher1_idx` (`teacher_idteacher` ASC, `teacher_student_id` ASC, `teacher_student_home_address` ASC) VISIBLE,
  CONSTRAINT `fk_control_has_group_control1`
    FOREIGN KEY (`control_id_control`)
    REFERENCES `mydb`.`control` (`id_control`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_control_has_group_group1`
    FOREIGN KEY (`group_group_number` , `group_specialitie_id_spec` , `group_semestr_idsemestr`)
    REFERENCES `mydb`.`group` (`group_number` , `specialitie_id_spec` , `semestr_idsemestr`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_control_has_group_teacher1`
    FOREIGN KEY (`teacher_idteacher` , `teacher_student_id` , `teacher_student_home_address`)
    REFERENCES `mydb`.`teacher` (`idteacher` , `student_id` , `student_home_address`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`group_has_teacher`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`group_has_teacher` (
  `group_group_number` INT(11) NOT NULL,
  `group_specialitie_id_spec` INT(11) NOT NULL,
  `group_semestr_idsemestr` INT(11) NOT NULL,
  `teacher_idteacher` INT(11) NOT NULL,
  `teacher_student_id` INT(11) NOT NULL,
  `teacher_student_home_address` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`group_group_number`, `group_specialitie_id_spec`, `group_semestr_idsemestr`, `teacher_idteacher`, `teacher_student_id`, `teacher_student_home_address`),
  INDEX `fk_group_has_teacher_teacher1_idx` (`teacher_idteacher` ASC, `teacher_student_id` ASC, `teacher_student_home_address` ASC) VISIBLE,
  INDEX `fk_group_has_teacher_group1_idx` (`group_group_number` ASC, `group_specialitie_id_spec` ASC, `group_semestr_idsemestr` ASC) VISIBLE,
  CONSTRAINT `fk_group_has_teacher_group1`
    FOREIGN KEY (`group_group_number` , `group_specialitie_id_spec` , `group_semestr_idsemestr`)
    REFERENCES `mydb`.`group` (`group_number` , `specialitie_id_spec` , `semestr_idsemestr`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_group_has_teacher_teacher1`
    FOREIGN KEY (`teacher_idteacher` , `teacher_student_id` , `teacher_student_home_address`)
    REFERENCES `mydb`.`teacher` (`idteacher` , `student_id` , `student_home_address`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`graduated_vnz_has_city`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`graduated_vnz_has_city` (
  `graduated_vnz_id` INT(11) NOT NULL,
  `city_city` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`graduated_vnz_id`, `city_city`),
  INDEX `fk_graduated_vnz_has_city_city1_idx` (`city_city` ASC) VISIBLE,
  INDEX `fk_graduated_vnz_has_city_graduated_vnz1_idx` (`graduated_vnz_id` ASC) VISIBLE,
  CONSTRAINT `fk_graduated_vnz_has_city_graduated_vnz1`
    FOREIGN KEY (`graduated_vnz_id`)
    REFERENCES `mydb`.`graduated_vnz` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_graduated_vnz_has_city_city1`
    FOREIGN KEY (`city_city`)
    REFERENCES `mydb`.`city` (`city`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
